"""
Stocker class TestCase
"""
import unittest
from machine.korbit_machine import KorbitMachine
import inspect
import configparser

class KorbitMachineTestCase(unittest.TestCase):
    """
    Test KorbitMachine
    """
    def setUp(self):
        config = configparser.ConfigParser()
        config.read('conf/config.ini')
        client_id = config['KORBIT']['client_id']
        client_secret = config['KORBIT']['client_secret']
        username = config['KORBIT']['username']
        password = config['KORBIT']['password']
        self.korbit_machine = KorbitMachine(client_id=client_id,
                      client_secret=client_secret,
                      username=username,
                      password=password)
        token = self.korbit_machine.set_token()

    def test_set_token(self):
        print(inspect.stack()[0][3])
        token = self.korbit_machine.set_token()
        assert token
        print(token)
        expire, access_token, refresh_token = self.korbit_machine.set_token(grant_type="password")
        assert access_token
        print(refresh_token)

    def test_get_ticker(self):
        """
        test get_ticker
        request_type = None, detailed
        """
        print(inspect.stack()[0][3])
        ticker = self.korbit_machine.get_ticker("etc_krw")
        assert ticker
        print(ticker)
        detailed_ticker = self.korbit_machine.get_ticker("etc_krw")
        assert detailed_ticker
        print(detailed_ticker)

    def test_get_filled_orders(self):
        """
        test get_filled_orders
        """
        print(inspect.stack()[0][3])
        order_book = self.korbit_machine.get_filled_orders(coin_type="btc_krw")
        print(order_book)
        assert order_book

    def test_get_constants(self):
        """
        test get_constants
        saved self.constants
        """
        print(inspect.stack()[0][3])
        constants = self.korbit_machine.get_constants()
        assert constants
        print(constants)

    def test_get_wallet_status(self):
        """
        test get_wallet_status
        """
        print(inspect.stack()[0][3])
        wallet_status = self.korbit_machine.get_wallet_status()
        assert wallet_status
        print(wallet_status)
        print("balance:"+wallet_status["krw"]["avail"])

    def test_get_list_my_orders(self):
        """
        test get_list_my_order
        """
        print(inspect.stack()[0][3])
        my_orders = self.korbit_machine.get_list_my_orders("btc_krw")
        assert my_orders
        print(my_orders)

    def test_get_my_order_status(self):
        """
        test get_list_my_order
        """
        print(inspect.stack()[0][3])
        my_order = self.korbit_machine.get_my_order_status("eth_krw",'8943263')
        assert my_order
        print(my_order)

    """
    def test_cacnel_coin_order(self):
        print(inspect.stack()[0][3])
        cancel_order = self.korbit_machine.cancel_coin_order(coin_type="etc_krw", order_id="5064611")
        assert cancel_order
        print(cancel_order)

    def test_buy_coin_order(self):
        print(inspect.stack()[0][3])
        buy_order = self.korbit_machine.buy_coin_order(coin_type="etc_krw", price="12000", qty="10", order_type="limit")
        assert buy_order
        print(buy_order)
    def test_sell_coin_order(self):
        print(inspect.stack()[0][3])
        sell_order = self.korbit_machine.sell_coin_order(coin_type="etc_krw", price="20000", qty="10", order_type="limit")
        assert sell_order
        print(sell_order)
    """
    def tearDown(self):
        pass

if __name__ == "__main__":
    print("test_korbit_machine")
    unittest.main()
